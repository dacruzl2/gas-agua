package com.dacruzl2.jerry_gasagua.Model.locationStatical;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


//(tableName = "Locations")

@Entity
public class LocationItems {

    private String numeroCasa;
    private String endereco;
    private String tvTitleLocation;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @Ignore
    public LocationItems() {

    }

    public LocationItems(String endereco, String tvTitleLocation) {
        this.endereco = endereco;
        this.tvTitleLocation = tvTitleLocation;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(String numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    public String getTvTitleLocation() {
        return tvTitleLocation;
    }

    public void setTvTitleLocation(String tvTitleLocation) {
        this.tvTitleLocation = tvTitleLocation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
