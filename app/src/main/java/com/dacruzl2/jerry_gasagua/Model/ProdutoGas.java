package com.dacruzl2.jerry_gasagua.Model;


public class ProdutoGas {


    private String marca;
    private String tempoMedio;
    private String preco;


    public ProdutoGas(String marca, String preco, String tempoMedio) {
        this.marca = marca;
        this.preco = preco;
        this.tempoMedio = tempoMedio;
    }

    public ProdutoGas() {
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getTempoMedio() {
        return tempoMedio;
    }

    public void setTempoMedio(String tempoMedio) {
        this.tempoMedio = tempoMedio;
    }
}
