package com.dacruzl2.jerry_gasagua.Model;

public class ProdutoAgua {

    private String litros;
    private String marca;
    private String tempoMedio;
    private String precoAgua;

    public ProdutoAgua(String litros, String marca, String tempoMedio) {
        this.litros = litros;
        this.marca = marca;
        this.tempoMedio = tempoMedio;
    }

    public ProdutoAgua() {
    }

    public String getLitros() {
        return litros;
    }

    public void setLitros(String litros) {
        this.litros = litros;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTempoMedio() {
        return tempoMedio;
    }

    public void setTempoMedio(String tempoMedio) {
        this.tempoMedio = tempoMedio;
    }

    public String getPrecoAgua() {
        return precoAgua;
    }

    public void setPrecoAgua(String precoAgua) {
        this.precoAgua = precoAgua;
    }
}
