package com.dacruzl2.jerry_gasagua.Model;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.dacruzl2.jerry_gasagua.Model.locationStatical.LocationItems;

@Database(entities = {LocationItems.class}, version = 1, exportSchema = false)
@TypeConverters({DataTypeConverter.class})
public abstract class LocationDatabase extends RoomDatabase {


    private static LocationDatabase locationDatabase;

    public abstract LocationDao getLocationDao();

    public static LocationDatabase getInstance(Context context) {
        if(locationDatabase == null)
            locationDatabase = Room.databaseBuilder(context.getApplicationContext(), LocationDatabase.class, "location_database")
                    .fallbackToDestructiveMigration()
                    .build();

        return locationDatabase;
    }

}


/*    private static volatile LocationDatabase INSTANCE;

    static LocationDatabase getDatabaseLocation(final Context context) {
        if (INSTANCE == null) {
            synchronized (LocationDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            LocationDatabase.class, "location_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }*/