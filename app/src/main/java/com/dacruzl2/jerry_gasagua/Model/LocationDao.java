package com.dacruzl2.jerry_gasagua.Model;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dacruzl2.jerry_gasagua.Model.locationStatical.LocationItems;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface LocationDao {

    @Query("SELECT * FROM LocationItems")
    public List<LocationItems> getAllLocations();

    @Query("SELECT * FROM LocationItems WHERE endereco = :endereco")
    public List<LocationItems> getLocationsByName(String endereco);

    @Insert(onConflict = REPLACE)
    public void insert(LocationItems locationItems);

    @Update
    public void update(LocationItems locationItems);

    @Delete
    public void delete(LocationItems locationItems);














}
  /*  @Insert
    void insertOnlySingleEndereco(LocationItems locationItems);

    @Query("SELECT * FROM location_table WHERE numeroCasa = :numeroCasa")
    LocationItems fetchLocationByNumeroCasa(int numeroCasa);

    @Update
    void updateLocation(LocationItems locationItems);
*/
//@Query("DELETE FROM location_table")
// void deleteLocationAll();


//@Query("SELECT * FROM location_table WHERE numeroCasa = :numeroCasa")
//public LocationItems getLocationWithnumeroCasa(int numeroCasa);

   /* @Query("SELECT * FROM LocationItems")
    public List<LocationItems> getLocations();

    @Insert
    public void insert(LocationItems... locationItems);

    @Update
    public void update(LocationItems... locationItems);

    @Delete
    public void delete(LocationItems locationItems);
*/
