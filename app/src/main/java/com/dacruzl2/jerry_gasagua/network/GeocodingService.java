package com.dacruzl2.jerry_gasagua.network;

import com.dacruzl2.jerry_gasagua.Model.CEP.CEP;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GeocodingService {

    public static final String API_KEY = "AIzaSyCpzyIDeXjuH-03q5WFNic3UtVvr5Ski_Y";

    //  https://maps.googleapis.com/maps/api/geocode/

    //address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyCpzyIDeXjuH-03q5WFNic3UtVvr5Ski_Y

    //"https://maps.googleapis.com/maps/api/geocode/";

    String URL_BASE = "https://viacep.com.br/ws/";

    @GET("SP/Jandira/{endereco}/json/")
    Call<List<CEP>> getAddress(@Path("endereco") String endereco);

}
