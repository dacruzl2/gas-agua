package com.dacruzl2.jerry_gasagua.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {


    public static Retrofit getRetrofit(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.build().retryOnConnectionFailure();
        httpClient.addInterceptor(interceptor);

        return new Retrofit.Builder()
                .baseUrl(GeocodingService.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
