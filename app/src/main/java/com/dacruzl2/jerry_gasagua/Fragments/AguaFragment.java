package com.dacruzl2.jerry_gasagua.Fragments;


import android.content.Intent;
import android.os.Bundle;


import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.dacruzl2.jerry_gasagua.Model.ProdutoAgua;

import com.dacruzl2.jerry_gasagua.R;
import com.dacruzl2.jerry_gasagua.activitys.AutenticaCelActivity;
import com.dacruzl2.jerry_gasagua.activitys.SelectProductsActivity;
import com.dacruzl2.jerry_gasagua.adapters.AguaAdapter;
import com.dacruzl2.jerry_gasagua.helpers.CustomLinearLayoutManager;
import com.dacruzl2.jerry_gasagua.listener.RecyclerItemClickListener;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AguaFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference produtosRef = db.collection("produtosAgua");

    private AguaAdapter AguaAdapter;
    private RecyclerView recyclerViewListaAgua;

    public AguaFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agua, container, false);

        mAuth = FirebaseAuth.getInstance();

        Query query = produtosRef.orderBy("precoAgua", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<ProdutoAgua> options = new FirestoreRecyclerOptions.Builder<ProdutoAgua>()
                .setQuery(query, ProdutoAgua.class)
                .build();
        AguaAdapter = new AguaAdapter(options);

        recyclerViewListaAgua = view.findViewById(R.id.rvAguaFragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewListaAgua.setHasFixedSize(true);
        recyclerViewListaAgua.setLayoutManager(layoutManager);
        recyclerViewListaAgua.setAdapter(AguaAdapter);
        recyclerViewListaAgua.addOnItemTouchListener(new RecyclerItemClickListener(
                getActivity(), recyclerViewListaAgua, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                AguaAdapter.getItem(position);

                if (mAuth.getCurrentUser() == null) {

                    Intent intent = new Intent(getActivity(), AutenticaCelActivity.class);
                    intent.putExtra("marca", AguaAdapter.getItem(position).getMarca());
                    intent.putExtra("tempoMedio", AguaAdapter.getItem(position).getTempoMedio());
                    intent.putExtra("preco", AguaAdapter.getItem(position).getPrecoAgua());
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(getActivity(), SelectProductsActivity.class);
                    intent.putExtra("marca", AguaAdapter.getItem(position).getMarca());
                    intent.putExtra("tempoMedio", AguaAdapter.getItem(position).getTempoMedio());
                    intent.putExtra("preco", AguaAdapter.getItem(position).getPrecoAgua());
                    startActivity(intent);
                }
            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }
        }
        ));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        AguaAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        AguaAdapter.stopListening();
    }
}
