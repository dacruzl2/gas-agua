package com.dacruzl2.jerry_gasagua.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.dacruzl2.jerry_gasagua.Model.ProdutoGas;
import com.dacruzl2.jerry_gasagua.R;
import com.dacruzl2.jerry_gasagua.activitys.AutenticaCelActivity;
import com.dacruzl2.jerry_gasagua.activitys.SelectProductsActivity;
import com.dacruzl2.jerry_gasagua.adapters.GasAdapter;
import com.dacruzl2.jerry_gasagua.listener.RecyclerItemClickListener;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class GasFragment extends Fragment {

    private RecyclerView recyclerViewListaGas;

    private FirebaseAuth mAuth;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference produtosRef = db.collection("produtosGas");

    private GasAdapter mainAdapter;

    public GasFragment(){}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gas, container, false);

        mAuth = FirebaseAuth.getInstance();

        Query query = produtosRef.orderBy("preco", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<ProdutoGas> options = new FirestoreRecyclerOptions.Builder<ProdutoGas>()
                .setQuery(query, ProdutoGas.class)
                .build();
        mainAdapter = new GasAdapter(options);

        recyclerViewListaGas = view.findViewById(R.id.rvGasFragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewListaGas.setLayoutManager(layoutManager);
        recyclerViewListaGas.setHasFixedSize(true);
        recyclerViewListaGas.setAdapter(mainAdapter);
        recyclerViewListaGas.addOnItemTouchListener(new RecyclerItemClickListener(
                getActivity(), recyclerViewListaGas, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mainAdapter.getItem(position);

                if (mAuth.getCurrentUser() == null) {

                    Intent intent = new Intent(getActivity(), AutenticaCelActivity.class);
                    intent.putExtra("marca", mainAdapter.getItem(position).getMarca());
                    intent.putExtra("tempoMedio", mainAdapter.getItem(position).getTempoMedio());
                    intent.putExtra("preco", mainAdapter.getItem(position).getPreco());
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(getActivity(), SelectProductsActivity.class);
                    intent.putExtra("marca", mainAdapter.getItem(position).getMarca());
                    intent.putExtra("tempoMedio", mainAdapter.getItem(position).getTempoMedio());
                    intent.putExtra("preco", mainAdapter.getItem(position).getPreco());
                    startActivity(intent);

                }
            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }
        }
        ));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mainAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        mainAdapter.stopListening();
    }
}
