package com.dacruzl2.jerry_gasagua.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dacruzl2.jerry_gasagua.Model.ProdutoAgua;
import com.dacruzl2.jerry_gasagua.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AguaAdapter extends FirestoreRecyclerAdapter<ProdutoAgua, AguaAdapter.AguaHolder> {


    public AguaAdapter(@NonNull FirestoreRecyclerOptions<ProdutoAgua> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull AguaHolder aguaHolder, int i, @NonNull ProdutoAgua produtoAgua) {
        aguaHolder.tvPrecoNumbersAgua.setText(produtoAgua.getPrecoAgua());
        aguaHolder.tvTempoMedioNumbersAgua.setText(produtoAgua.getTempoMedio());
        aguaHolder.tvMarcaAgua.setText(produtoAgua.getMarca());
    }

    @NonNull
    @Override
    public AguaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_agua_list, parent, false);

        return new AguaAdapter.AguaHolder(v);
    }

    class AguaHolder extends RecyclerView.ViewHolder {

        TextView tvMarcaAgua, tvTempoMedioAgua, tvPrecoAgua, tvTempoMedioNumbersAgua,
                tvPrecoNumbersAgua;

        AguaHolder(@NonNull View itemView) {
            super(itemView);

            tvMarcaAgua = itemView.findViewById(R.id.tvMarcaAgua);
            tvTempoMedioAgua = itemView.findViewById(R.id.tvTempoMedioAgua);
            tvPrecoAgua = itemView.findViewById(R.id.tvPrecoAgua);
            tvTempoMedioNumbersAgua = itemView.findViewById(R.id.tvTempoMedioNumbersAgua);
            tvPrecoNumbersAgua = itemView.findViewById(R.id.tvPrecoNumbersAgua);
        }
    }
}
