package com.dacruzl2.jerry_gasagua.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dacruzl2.jerry_gasagua.Model.ProdutoGas;
import com.dacruzl2.jerry_gasagua.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GasAdapter extends FirestoreRecyclerAdapter<ProdutoGas, GasAdapter.MainHolder> {


     public GasAdapter(@NonNull FirestoreRecyclerOptions<ProdutoGas> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull MainHolder mainHolder, int i, @NonNull ProdutoGas produto) {
        mainHolder.tvPrecoNumbers.setText(produto.getPreco());
        mainHolder.tvTempoMedioNumbers.setText(produto.getTempoMedio());
        mainHolder.tvRotate.setText(produto.getMarca());
    }

    @NonNull
    @Override
    public MainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gas_list, parent, false);

        return new MainHolder(v);
    }

    class MainHolder extends RecyclerView.ViewHolder {
        TextView tvTempoMedio, tvPreco, tvTempoMedioNumbers, tvPrecoNumbers, tvRotate;

        MainHolder(@NonNull View itemView) {
            super(itemView);

            tvTempoMedio = itemView.findViewById(R.id.tvTempoMedio);
            tvPreco = itemView.findViewById(R.id.tvPreco);
            tvTempoMedioNumbers = itemView.findViewById(R.id.tvTempoMedioNumbers);
            tvPrecoNumbers = itemView.findViewById(R.id.tvPrecoNumbers);
            tvRotate = itemView.findViewById(R.id.tvRotate);

        }
    }
}