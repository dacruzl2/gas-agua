package com.dacruzl2.jerry_gasagua.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dacruzl2.jerry_gasagua.Model.CEP.CEP;
import com.dacruzl2.jerry_gasagua.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchLocationAdapter extends RecyclerView.Adapter<SearchLocationAdapter.MyViewHolder> {


    private Context context;
    private List<CEP> geocodingList;

    public SearchLocationAdapter(Context context, List<CEP> geocodingList) {
        this.context = context;
        this.geocodingList = geocodingList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.geocoding_item_list, parent, false);

        return new SearchLocationAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final CEP geocodingItem = geocodingList.get(position);

        holder.tvAddress.setText(geocodingItem.getLogradouro());


    }

    @Override
    public int getItemCount() {
        return geocodingList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvRua,tvAddress;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvRua = itemView.findViewById(R.id.tvRua);
            tvAddress = itemView.findViewById(R.id.tvAddress);
        }
    }
}
