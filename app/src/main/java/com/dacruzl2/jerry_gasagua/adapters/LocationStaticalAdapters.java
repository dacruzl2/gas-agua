package com.dacruzl2.jerry_gasagua.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.dacruzl2.jerry_gasagua.Model.locationStatical.LocationItems;
import com.dacruzl2.jerry_gasagua.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationStaticalAdapters extends RecyclerView.Adapter<LocationStaticalAdapters.MyViewHolder> {


    private Context context;
    private List<LocationItems> locationItemsList;

    public LocationStaticalAdapters(Context context, List<LocationItems> locationItemsList) {
        this.context = context;
        this.locationItemsList = locationItemsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_adress_item_statical, parent, false);

        return new LocationStaticalAdapters.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LocationItems locationItems = locationItemsList.get(position);

        holder.tvEnderecoStatical.setText(locationItems.getEndereco());
        holder.tvTitleLocationStatical.setText(locationItems.getTvTitleLocation());

        //holder.lottieAnimationView.setVisibility(View.INVISIBLE);
        //holder.ivLocationStatical.setVisibility(View.INVISIBLE);


    }

    @Override
    public int getItemCount() {
        return locationItemsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvEnderecoStatical)
        TextView tvEnderecoStatical;
        @BindView(R.id.tvTitleLocationStatical)
        TextView tvTitleLocationStatical;

        @BindView(R.id.loading_animationStatical)
        LottieAnimationView lottieAnimationView;

        @BindView(R.id.ivLocationStatical)
        ImageView ivLocationStatical;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }

    }
}
