package com.dacruzl2.jerry_gasagua.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dacruzl2.jerry_gasagua.Model.ProdutoAgua;
import com.dacruzl2.jerry_gasagua.R;
import com.dacruzl2.jerry_gasagua.adapters.AguaAdapter;
import com.dacruzl2.jerry_gasagua.helpers.CustomLinearLayoutManager;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class AutenticaCelActivity extends AppCompatActivity {


    EditText edtPhoneNumber, edtDdd;

    private TextView tvTempoMedio, tvPrecoNumbers, tvRotate;

    private RecyclerView recyclerViewHorizontal;
    private AguaAdapter AguaAdapter;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference produtosRef = db.collection("produtosAgua");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autentica_cel);

        mAuth = FirebaseAuth.getInstance();

        userStatusLogin();

        Query query = produtosRef.orderBy("precoAgua", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<ProdutoAgua> options = new FirestoreRecyclerOptions.Builder<ProdutoAgua>()
                .setQuery(query, ProdutoAgua.class)
                .build();
        AguaAdapter = new AguaAdapter(options);


        recyclerViewHorizontal = findViewById(R.id.rvHorizontalAutenticaCell);
       /* RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);*/
        recyclerViewHorizontal.setLayoutManager(new CustomLinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        recyclerViewHorizontal.setHasFixedSize(true);
        recyclerViewHorizontal.setAdapter(AguaAdapter);

        final int speedScroll = 2400;
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            int count = 0;
            boolean flag = true;

            @Override
            public void run() {
                if (count < AguaAdapter.getItemCount()) {
                    if (count == AguaAdapter.getItemCount() - 1) {
                        flag = false;
                    } else if (count == 0) {
                        flag = true;
                    }
                    if (flag) count++;
                    else count--;

                    recyclerViewHorizontal.smoothScrollToPosition(count);
                    handler.postDelayed(this, speedScroll);
                }
            }
        };

        handler.postDelayed(runnable, speedScroll);


        tvTempoMedio = findViewById(R.id.tvTempoMedioNumbers);
        tvPrecoNumbers = findViewById(R.id.tvPrecoNumbers);
        tvRotate = findViewById(R.id.tvRotate);

        Intent it = getIntent();

        //Recuperei a string da outra activity
        String marca = it.getStringExtra("marca");
        String tempoMedio = it.getStringExtra("tempoMedio");
        String preco = it.getStringExtra("preco");

        tvTempoMedio.setText(tempoMedio);
        tvPrecoNumbers.setText(preco);
        tvRotate.setText(marca);


        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        edtDdd = findViewById(R.id.edtDdd);

        findViewById(R.id.btProximo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ddd = edtDdd.getText().toString();
                String phoneNumber = edtPhoneNumber.getText().toString();

                if (ddd.isEmpty() || ddd.length() < 2) {

                    edtDdd.setError("ddd é requerido");
                    edtDdd.requestFocus();
                    return;
                }

                if (phoneNumber.isEmpty() || phoneNumber.length() < 9) {

                    edtPhoneNumber.setError("Numero do telefone requerido");
                    edtPhoneNumber.requestFocus();
                    return;
                }


                String numberGetPhone = "+55" + ddd + phoneNumber;

                Intent intent = new Intent(AutenticaCelActivity.this, VerifyPhoneActivity.class);
                intent.putExtra("phoneNumber", numberGetPhone);
                startActivity(intent);

            }
        });
    }

    private void userStatusLogin() {

        if (mAuth.getCurrentUser() != null) {

            startActivity(new Intent(getApplicationContext(), MainActivity.class));

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        AguaAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        AguaAdapter.stopListening();

    }
}