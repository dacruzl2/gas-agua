package com.dacruzl2.jerry_gasagua.activitys;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.dacruzl2.jerry_gasagua.BuildConfig;
import com.dacruzl2.jerry_gasagua.Model.CEP.CEP;
import com.dacruzl2.jerry_gasagua.Model.LocationDao;
import com.dacruzl2.jerry_gasagua.Model.LocationDatabase;
import com.dacruzl2.jerry_gasagua.Model.locationStatical.LocationItems;
import com.dacruzl2.jerry_gasagua.R;
import com.dacruzl2.jerry_gasagua.adapters.LocationStaticalAdapters;
import com.dacruzl2.jerry_gasagua.adapters.SearchLocationAdapter;
import com.dacruzl2.jerry_gasagua.helpers.MyDividerItemDecoration;
import com.dacruzl2.jerry_gasagua.listener.RecyclerItemClickListener;
import com.dacruzl2.jerry_gasagua.network.GeocodingService;
import com.dacruzl2.jerry_gasagua.network.RetrofitConfig;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LocationAdressActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.location_address_layout)
    ConstraintLayout location_address_layout;

    LottieAnimationView lottieAnimationView;

    ImageView ivLocationStatical;

    TextView enderecoStatical, myUseLocation;

    SearchView search;

    RecyclerView rvLocationSearch;
    RecyclerView rvPrincipal;

    // location last updated time
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private static final int REQUEST_CHECK_SETTINGS = 100;

    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;

    private Address address;

    private SearchLocationAdapter searchLocationAdapter;
    private LocationStaticalAdapters locationStaticalAdapters;
    private List<CEP> geocodingList;
    private List<LocationItems> locationItemsList;

    LinearLayoutManager mLayoutManager;

    private Retrofit retrofit;

    private LocationItems locationItems = new LocationItems();


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_adress);
        ButterKnife.bind(this);

        search = findViewById(R.id.search);
        search.requestFocus();

        geocodingList = new ArrayList<>();
        locationItemsList = new ArrayList<>();

        retrofit = RetrofitConfig.getRetrofit();

        mLayoutManager = new LinearLayoutManager(this);
        rvLocationSearch = findViewById(R.id.rvLocationSearch);
        searchLocationAdapter = new SearchLocationAdapter(this, geocodingList);
        rvLocationSearch.setHasFixedSize(true);
        rvLocationSearch.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        rvLocationSearch.setLayoutManager(mLayoutManager);
        rvLocationSearch.setAdapter(searchLocationAdapter);

        rvPrincipal = findViewById(R.id.rvPrincipal);
        mLayoutManager = new LinearLayoutManager(this);
        locationStaticalAdapters = new LocationStaticalAdapters(this, locationItemsList);
        rvPrincipal.setHasFixedSize(true);
        rvPrincipal.setLayoutManager(mLayoutManager);
        rvPrincipal.setAdapter(locationStaticalAdapters);
        locationItems.setEndereco("Encontraremos você para entregar o seu produto ;)");
        locationItems.setTvTitleLocation("Use minha localização");
        locationItemsList.add(0, locationItems);
        locationStaticalAdapters.notifyItemInserted(0);
        rvPrincipal.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 50));
        rvPrincipal.addOnItemTouchListener(new RecyclerItemClickListener(this, rvPrincipal,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        lottieAnimationView = view.findViewById(R.id.loading_animationStatical);
                        ivLocationStatical = view.findViewById(R.id.ivLocationStatical);
                        enderecoStatical = view.findViewById(R.id.tvEnderecoStatical);
                        myUseLocation = view.findViewById(R.id.tvTitleLocationStatical);

                        Toast.makeText(LocationAdressActivity.this, "click " + position, Toast.LENGTH_SHORT).show();

                        if (locationItems.getEndereco()
                                .contains("Encontraremos você para entregar o seu produto ;)")
                                || locationItems.getTvTitleLocation().contains("Precisamos que você ative a localização!")) {

                            enderecoStatical.setText("");
                            myUseLocation.setText("");

                            if (position == 0) {
                                locationItemsList.clear();
                                locationStaticalAdapters.notifyDataSetChanged();

                                locationItems.setTvTitleLocation("Procurando por sua localização");
                                locationItems.setEndereco("Isso pode levar um momento");
                                locationItemsList.add(locationItems);

                                ivLocationStatical.setVisibility(View.INVISIBLE);
                                lottieAnimationView.setVisibility(View.VISIBLE);
                                locationStaticalAdapters.notifyItemChanged(position);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startLocationButtonClick();
                                    }

                                }, 5000);

                            }
                        } else if (position == 0 && !locationItems.getTvTitleLocation().equals("Procurando por sua localização")) {

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder
                                    (LocationAdressActivity.this);

                            View dialogView = getLayoutInflater().inflate(R.layout.custom_location_alert_dialog, null);

                            TextView tvRua = dialogView.findViewById(R.id.tvRua);

                            tvRua.setText(address.getAddressLine(0));

                            EditText edtNumeroCasaCustomDialog = dialogView.findViewById(R.id.edtNumeroCasaCustomDialog);
                            EditText edtComplementoCustomDialog = dialogView.findViewById(R.id.edtComplementoCustomDialog);


                            alertDialogBuilder.setTitle("Informe o número da casa");
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                @SuppressLint("StaticFieldLeak")
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alertDialogBuilder.setView(dialogView);

                            AlertDialog dialogAlert = alertDialogBuilder.create();
                            dialogAlert.show();
                            dialogAlert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @SuppressLint("StaticFieldLeak")
                                @Override
                                public void onClick(View v) {
                                    String numeroCasa = edtNumeroCasaCustomDialog.getText().toString();

                                    if (numeroCasa.isEmpty()) {

                                        edtNumeroCasaCustomDialog.setError("Obrigatório");
                                        edtNumeroCasaCustomDialog.requestFocus();

                                        return;
                                    }
                                    new AsyncTask<Void, Void, Void>() {
                                        @Override
                                        protected Void doInBackground(Void... voids) {
                                            LocationDao locationDao = LocationDatabase.getInstance(LocationAdressActivity.this)
                                                    .getLocationDao();

                                            locationItems.getEndereco();
                                            locationItems.setNumeroCasa(edtNumeroCasaCustomDialog.getText().toString());

                                            locationDao.insert(locationItems);

                                            return null;
                                        }

                                    }.execute();
                                    stopLocationUpdates();
                                    dialogAlert.dismiss();


                                }
                            });

                        } else {
                            Toast.makeText(LocationAdressActivity.this, "Favor aguardar!", Toast.LENGTH_SHORT).show();

                        }
                        stopLocationUpdates();

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                }));


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.length() >= 4) {
                    getAddressSearch(query);
                    rvPrincipal.setVisibility(View.INVISIBLE);
                    rvLocationSearch.setVisibility(View.VISIBLE);

                } else if (query.length() < 3) {
                    geocodingList.clear();
                    searchLocationAdapter.notifyDataSetChanged();
                    rvPrincipal.setVisibility(View.VISIBLE);
                    rvLocationSearch.setVisibility(View.GONE);
                }
                return true;
            }
        });

        search.setQueryHint("Buscar Endereço");

        location_address_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(v);
                search.clearFocus();
                return true;
            }
        });

        // initialize the necessary libraries
        init();

        // restore the values from saved instance state
        restoreValuesFromBundle(savedInstanceState);

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI();
            }

        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {

            mCurrentLocation.getLatitude();
            mCurrentLocation.getLongitude();

            try {
                address = getAddress(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

                locationItemsList.remove(locationItems);
                locationStaticalAdapters.notifyItemRemoved(0);

                locationItems.setEndereco(address.getSubLocality() + address.getAddressLine(0));
                locationItems.setTvTitleLocation("Localização atual");
                locationItemsList.add(locationItems);

                stopLocationUpdates();
                locationStaticalAdapters.notifyItemInserted(0);


            } catch (IOException e) {

                Toast.makeText(this, "Tente procurar novamente: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("GPS", e.getMessage());
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }

    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");


                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(LocationAdressActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(LocationAdressActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();

                        locationItemsList.remove(0);
                        locationStaticalAdapters.notifyItemRemoved(0);

                        locationItems.setTvTitleLocation("Precisamos que você ative a localização!");
                        locationItems.setEndereco("");
                        locationItemsList.add(locationItems);

                        lottieAnimationView.setVisibility(View.GONE);
                        ivLocationStatical.setVisibility(View.VISIBLE);

                        locationStaticalAdapters.notifyDataSetChanged();

                    }
                });
    }

    // @OnClick(R.id.btn_start_location_updates)
    public void startLocationButtonClick() {
        // Requesting ACCESS_FINE_LOCATION using Dexter library
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                        //  toggleButtons();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check for the integer request code originally supplied to startResolutionForResult().
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.e(TAG, "User agreed to make required location settings changes.");
                    // Nothing to do. startLocationupdates() gets called in onResume again.
                    break;
                case Activity.RESULT_CANCELED:
                    Log.e(TAG, "User chose not to make required location settings changes.");
                    mRequestingLocationUpdates = false;
                    break;
            }
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        search.setIconifiedByDefault(false);

    }

    @Override
    public void onResume() {
        super.onResume();
        // Resuming location updates depending on button state and
        // allowed permissions
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }

    public Address getAddress(double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        Address address = null;
        List<Address> addresses;

        geocoder = new Geocoder(getApplicationContext());
        addresses = geocoder.getFromLocation(latitude, longitude, 1);

        if (addresses.size() > 0) {
            address = addresses.get(0);
        }
        return address;
    }

    public void getAddressSearch(String endereco) {
        GeocodingService geocodingService = retrofit.create(GeocodingService.class);
        geocodingService.getAddress(endereco)
                .enqueue(new Callback<List<CEP>>() {
                    @Override
                    public void onResponse(Call<List<CEP>> call, Response<List<CEP>> root) {
                        if (root.isSuccessful()) {
                            Log.d("LOG", "resultado: " + root.toString());
                            assert root.body() != null;

                            geocodingList.clear();
                            geocodingList.addAll(root.body());
                            searchLocationAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<CEP>> call, Throwable t) {
                        Toast.makeText(LocationAdressActivity.this, "Conexão falhou!!!!", Toast.LENGTH_SHORT).show();
                    }
                });
    }


}