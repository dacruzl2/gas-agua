package com.dacruzl2.jerry_gasagua.activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.dacruzl2.jerry_gasagua.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class IniciarActivity extends AppCompatActivity {


    ImageView ivEmpresaJerry;
    EditText edtNome;
    Button btProximo;

    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar);


        ivEmpresaJerry = findViewById(R.id.ivEmpresaJerry);
        edtNome = findViewById(R.id.edtNome);
        btProximo = findViewById(R.id.btProximo);


        btProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

            }
        });


    }

}

/*
obter user
    DatabaseReference usuarios = reference;

                usuarios.addValueEventListener(new ValueEventListener() {
@Override
public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        Log.i("FIREBASE", dataSnapshot.getValue().toString());
        }

@Override
public void onCancelled(@NonNull DatabaseError databaseError) {

        }*/

//Salvauser
    /* usuario.setTelefone("11947646166");
                usuario.setNome(edtNome.getText().toString());
                    Usuario usuario = new Usuario();
                usuarios.child("001").setValue(usuario);*/
