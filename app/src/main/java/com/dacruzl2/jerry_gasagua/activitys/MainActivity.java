package com.dacruzl2.jerry_gasagua.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;


import com.dacruzl2.jerry_gasagua.Model.ProdutoGas;
import com.dacruzl2.jerry_gasagua.R;
import com.dacruzl2.jerry_gasagua.adapters.GasAdapter;
import com.dacruzl2.jerry_gasagua.listener.RecyclerItemClickListener;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;


public class MainActivity extends AppCompatActivity {

    public RecyclerView recyclerView;
    public GasAdapter mainAdapter;
    LinearLayoutManager mLayoutManager;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference produtosRef = db.collection("produtos");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        Query query = produtosRef.orderBy("preco", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<ProdutoGas> options = new FirestoreRecyclerOptions.Builder<ProdutoGas>()
                .setQuery(query, ProdutoGas.class)
                .build();

        mainAdapter = new GasAdapter(options);
        mLayoutManager = new LinearLayoutManager(this);

        recyclerView = findViewById(R.id.recyclerViewMain);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mainAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(
                this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mainAdapter.getItem(position);

                if (mAuth.getCurrentUser() == null) {

                    Intent intent = new Intent(getApplicationContext(), AutenticaCelActivity.class);
                    intent.putExtra("marca", mainAdapter.getItem(position).getMarca());
                    intent.putExtra("tempoMedio", mainAdapter.getItem(position).getTempoMedio());
                    intent.putExtra("preco", mainAdapter.getItem(position).getPreco());
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(MainActivity.this, SelectProductsActivity.class);
                    intent.putExtra("marca", mainAdapter.getItem(position).getMarca());
                    intent.putExtra("tempoMedio", mainAdapter.getItem(position).getTempoMedio());
                    intent.putExtra("preco", mainAdapter.getItem(position).getPreco());

                    startActivity(intent);
                }
            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }
        }
        ));
    }

    @Override
    protected void onStart() {
        super.onStart();

        mainAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mainAdapter.stopListening();

    }

    private void userStatusLogin(int position) {

        if (mAuth.getCurrentUser() == null) {

            Intent intent = new Intent(getApplicationContext(), AutenticaCelActivity.class);
            intent.putExtra("marca", mainAdapter.getItem(position).getMarca());
            intent.putExtra("tempoMedio", mainAdapter.getItem(position).getTempoMedio());
            intent.putExtra("preco", mainAdapter.getItem(position).getPreco());
            startActivity(intent);

            finish();

        }
    }


}

