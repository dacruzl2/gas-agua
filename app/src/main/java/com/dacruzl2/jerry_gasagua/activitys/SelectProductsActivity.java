package com.dacruzl2.jerry_gasagua.activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dacruzl2.jerry_gasagua.R;

public class SelectProductsActivity extends AppCompatActivity {

    TextView tvCopagaz, tvOpenClosed, tvTempoMedioNumbersSelectActivity, tvCopagazSelector,
            tvDecrementor, tvBotijaoKg, tvIncrementor, tvPriceSelector, tvJerryGaz;

    ImageView ivGas, ivEmpresa;
    Button btIrParaPagamento;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_products);

        initComponents();

        //Pega a intent de outra activity
        Intent it = getIntent();
        //Recuperei a string da outra activity
        String marca = it.getStringExtra("marca");
        String tempoMedio = it.getStringExtra("tempoMedio");
        String preco = it.getStringExtra("preco");

        tvPriceSelector.setText(preco);
        tvCopagazSelector.setText(marca);
        tvTempoMedioNumbersSelectActivity.setText(tempoMedio);
    }

    public void initComponents() {

        tvCopagaz = findViewById(R.id.tvCopagaz);
        tvOpenClosed = findViewById(R.id.tvOpenClosed);
        tvTempoMedioNumbersSelectActivity = findViewById(R.id.tvTempoMedioNumbersSelectActivity);
        tvCopagazSelector = findViewById(R.id.tvCopagazSelector);
        tvDecrementor = findViewById(R.id.tvDecrementor);
        tvBotijaoKg = findViewById(R.id.tvBotijaoKg);
        tvIncrementor = findViewById(R.id.tvIncrementor);
        tvPriceSelector = findViewById(R.id.tvPriceSelector);
        tvJerryGaz = findViewById(R.id.tvJerryGaz);
        ivGas = findViewById(R.id.ivGas);
        ivEmpresa = findViewById(R.id.ivEmpresa);
        btIrParaPagamento = findViewById(R.id.bt_irParaPagamento);
    }


}
